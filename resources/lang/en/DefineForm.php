<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [
    "username"=>"UserName",
    "email"=>"Email Address",
    "address"=>"Address",
    "phone"=>"Phone",
    "sex"=>"Sex",
    "male"=>"Male",
    "female"=>"Female",
    "login"=>"Login",
    "password"=>"Password",
    "forgot"=>"Forgot Your Password?",
    "confirm"=>"Confirm Password",
    "create"=>"Create",
    "list"=>"List",
    "add"=>"Add",
    "register"=>"Register",
    "name"=>"Name"
];

