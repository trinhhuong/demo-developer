@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">New Article</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="post" action="{{route('post',['id'=>Auth::user()->id])}}" enctype="multipart/form-data">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" value="{{ old('title') }}">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title')}}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="description" value="{{ old('description') }}">

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('detail') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Content</label>

                            <div class="col-md-6">
                                <textarea style="height: 100px"  class="form-control" name="detail" >{{old('detail')}}</textarea>

                                @if ($errors->has('detail'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('detail') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
                    
                    <label class="col-md-4 control-label">Upload Avatar</label>
                     <div class='col-md-6'>
                         <input style="width:70%"class='form-control' id="uploadFile" placeholder="Choose File" disabled="disabled" >
                    <label  class="custom-file-input control-label" >
                         <input type="file" id="uploadBtn" name="file" style="padding-top:5px">
                    </label>
                    @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file')}}</strong>
                                    </span>
                                @endif
                     </div>
                </div>    
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Post
                                </button>
                            </div>
                            </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    
</div>
<script src="{{ asset('js/style.js') }}"></script>
@endsection
