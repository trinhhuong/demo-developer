@extends('layouts.app')
@section('content')
<div class='col-xs-8 col-xs-offset-2'>
            <form class='form-horizontal' action="{{route('update',['id'=>$demo->id])}}" method="Post">
                <input type='hidden' name="_token" value='{{csrf_token()}}'>
                <div class='form-group {{ $errors->has('name') ? ' has-error' : '' }}'>
                    <label>{{trans('DefineForm.name')}}</label>
                    <input type='text' class='form-control' value="{{$demo->name}}" name='name'>
                     @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name')}}</strong>
                                    </span>
                                @endif
                </div>
                <div class='form-group {{ $errors->has('email') ? ' has-error' : '' }}'>
                    <label>{{trans('DefineForm.email')}}</label>
                    <input type='text' class='form-control' value="{{$demo->email}}" name='email'>
                     @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>

                @if(Auth::user()->role==Config::get('constant.SUPERADMIN'))
                <div class='form-group'>
                    <label>Role</label>
                    <select name='role' class='form-control'>
                       
                        <option value={{Config::get('constant.ADMIN')}} @if($demo->role==Config::get('constant.ADMIN')) {{"selected='selected'"}} @endif>Admin</option>
                        <option value={{Config::get('constant.MEMBER')}} @if($demo->role==Config::get('constant.MEMBER')) {{"selected='selected'"}} @endif>Member</option>        
                    </select>
                </div>
                @endif
                @if(Auth::user()->role==Config::get('constant.ADMIN'))
                
                 <select  style="display: none" name='role' class='form-control'>
                        <option value={{Config::get('constant.MEMBER')}}  @if($demo->role==Config::get('constant.MEMBER')) {{"selected='selected'"}} @endif>Member</option>        
                    </select>
               
                @endif
                <button type="submit" class='btn btn-default'>Edit</button>
            </form>
        </div>
@endsection 

