@extends('layouts.app')
@section('content')
    <div class="container">
      <div class="row">
      
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >
   
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">{{$demo->name}}</h3>
              @if(Auth::user()->id==$demo->id)
              <span style="margin: -25px 10px"class="pull-right">
                    <a href="{{route('editprofile',['id'=>$demo->id])}}" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                            
              </span>
              @endif
            </div>
            <div class="panel-body">
              <div class="row">
                  <div class="col-md-3 col-lg-3 " align="center"> <img src="{{asset('uploads/')}}/{{$profile->avatar}}"  class="img-rounded img-responsive"> </div>
                
               
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td>{{$demo->name}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{$demo->email}}</td>
                        </tr>
                        <tr>
                            <td>Birthday</td>
                            <td>{{$profile->dob}}</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>{{$profile->phone}}</td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>{{$profile->address}}</td>
                        </tr>
                    </tbody>
                  </table>
                  
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
