<?php

namespace App\Http\Middleware;

use Closure;
use App\Article;
use Illuminate\Support\Facades\Config;
class IsAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //var_dump(Article::findById($request->route()->getParameter('id'))->user_id);die;
        if($request->user()->role==Config::get('constant.SUPERADMIN') or $request->user()->id==Article::findById($request->route()->getParameter('id'))->user_id){
        return $next($request);}
        else return redirect ('/');
    }
}
