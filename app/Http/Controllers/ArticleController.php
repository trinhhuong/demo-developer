<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ArticleRequest;
use App\Article;
use App\Demo;
use Illuminate\Support\Facades\Config;
class ArticleController extends Controller
{
    public function viewpost() {
        return view('article.create');
    }
    public function post(ArticleRequest $request,$id){
        
        if($request->hasFile('file')){
            $ava=$request->file('file');
            $name=$ava->getClientOriginalName();
            $ava->move('uploads',$name);
           
        }
                        
        else $name=  Config::get('constant.avatar');

        
        $input=[
            'user_id'=>$id,
            'title'=>$request['title'],
            'description'=>$request['description'],
            'detail'=>$request['detail'],
            'image'=>$name,
        ];
        Article::insert($input);
        return redirect()->route('listarticle',['id'=>$id]);
    }
    public function listpost($id){
        $demo=Demo::findById($id);
        $articles=$demo->articles;
        
        //var_dump($articles);die;
        
        return view('article.list',['articles'=>$articles]);
        
    }
    public function detail($user_id,$article_id) {
        //var_dump($article_id);die;
        $user=Demo::findById($user_id);
        $news=Article::findById($article_id);
        return view('article.detail',['article'=>$news,'demo'=>$user]);
    }
    public function articles(){
        $articles=Article::getAll();
      
        return view('welcome',['articles'=>$articles]);
    }
    public function edit($id){
        $article=Article::findById($id);
        return view('article.edit',['article'=>$article]);
    }
    public function postedit(ArticleRequest $request,$id){
        $article=Article::findById($id);
        if($request->hasFile('file')){
            $ava=$request->file('file');
            $name=$ava->getClientOriginalName();
            $ava->move('uploads',$name);}
        else $name= $article->image; 
            $input=[
                'user_id'=>$article->user->id,
                'title'=>$request['title'],
                'description'=>$request['description'],
                'detail'=>$request['detail'],
                'image'=>$name,
            ];
            
        Article::updateById($input,$id);
        $article=Article::findById($id);    
        
         return view('article.detail',['article'=>$article,'demo'=>$article->user]);
    }
    public function delete($id){
        $demo=Article::findById($id)->user->id;
        Article::deleteById($id);
        $articles=Demo::findById($demo)->articles;
        //var_dump($articles);die;
       
        return view('article.list',['articles'=>$articles]);
        
    }
}
