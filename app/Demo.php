<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    protected $table='users';
    public $timestamp=false;
    protected $fillable=['name','role','email','password','remember_token'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function getAll(){
        $demo=Demo::all();
        return $demo;
    }
    public static function insert($input){
      $demo=new Demo();
      $demo->fill($input);
      $demo->save();
    }
    public static function findById($id){
        $demo=Demo::find($id);
        return $demo;
    }
    public static function updateById($input ,$id){
        $demo=Demo::findById($id);
        $demo->fill($input);
        $demo->save();
    }
    public static function deleteById($id){
       $demo=Demo::findById($id);
       $demo->delete();
    }
   
            
    public function profile(){
        return $this->hasOne('App\Profile','user_id');
    }      
    public function articles() {
        return $this->hasMany('App\Article','user_id');
    }
}
