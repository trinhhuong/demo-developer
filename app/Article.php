<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Article extends Model
{
    protected $table='articles';
    public $timestamp=false;
    protected $fillable=['user_id','title','description','detail','image'];
    public static function insert($input){
        $article=new Article();
        $article->fill($input);
        $article->save();
    }
    public static function findById($id){
        $article= Article::find($id);
        return $article;
    } 
    public static function getAll(){
        $article=Article::orderBy('updated_at','desc')->get();
        return $article;
    }
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
     public static function updateById($input,$id) {
        $demo=  Article::findById($id);
        $demo->fill($input);
        $demo->save();
    }
    public static function deleteById($id){
       $demo=Article::findById($id);
       $demo->delete();
    }
}
